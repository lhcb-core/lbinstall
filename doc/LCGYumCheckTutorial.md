LCG Yum repository consistency check
====================================

In order to verify the consistency of a LCG version, you can run:

* `cd /cvmfs/lhcbdev.cern.ch/TODO_PATH/`
* `lcgyumcheck [LCG_VERSION]`

