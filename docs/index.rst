.. lbinstall documentation master file, created by
   sphinx-quickstart on Mon Feb 20 14:58:15 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to lbinstall's documentation!
=====================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   Lbinstall_main
   Installation
   Usage
   Design
   lbinstall


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
