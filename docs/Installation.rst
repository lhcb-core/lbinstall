Installation
============

Try it on lxplus:

::

    virtualenv install
    source install/bin/activate
    pip install lbinstall
    lbinstall --root=$TMPDIR/myroot install DAVINCI_v42r1_x86_64_slc6_gcc62_opt
